use crate::entities::hex::HexCell;
use crate::GameState;
use crate::{entities::hex, entities::ship::*, MouseWorldPos, ShipDirVal, ShipList, Tile};
use bevy::{input::mouse::MouseWheel, prelude::*};

pub struct NextShipEvent;
pub struct SpawnShipEvent {
    ship_pos: Vec3,
    ship_dir: ShipDirEnum,
    ship_texture: Handle<Texture>,
    ship_type: ShipEnum,
}

pub fn rotate_ship(mut mouse_wheel: EventReader<MouseWheel>, mut dir: ResMut<ShipDirVal>) {
    for ev in mouse_wheel.iter() {
        let s = dir.0 + ev.x as i32;
        dir.0 += s % 6;
        println!("{:?}", dir.0);
    }
}

pub fn spawn_next_ship(
    mut commands: Commands,
    mouse_pos: Res<MouseWorldPos>,
    asset_server: Res<AssetServer>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut next_ship_event: EventReader<NextShipEvent>,
    mut ships_available: ResMut<ShipList>,
    mut app_state: ResMut<State<GameState>>,
) {
    for _ev in next_ship_event.iter() {
        if let Some(ship) = ships_available.0.pop() {
            println!("{:?}", ship);
            let idx = hex::to_idx(mouse_pos.0);
            let hex_pos = hex::HexCell::new(idx.x as i32, idx.y as i32, 4);
            let ship_tex: Handle<Texture> = asset_server.get_handle("GameHex/ship_2tiles_dwl.png");
            commands
                .spawn_bundle(SpriteBundle {
                    material: materials.add(ship_tex.into()),
                    transform: Transform::from_translation(hex_pos.pos),
                    ..Default::default()
                })
                .insert(ShipNotPlaced);
        } else {
            app_state.set(GameState::InGame).unwrap();
        }
    }
}

pub fn hover_with_ship(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mouse_pos: Res<MouseWorldPos>,
    mouse_button_input: Res<Input<MouseButton>>,
    mut q: Query<&mut Transform, With<ShipNotPlaced>>,
    entity: Query<Entity, With<ShipNotPlaced>>,
    mut next_ship: EventWriter<NextShipEvent>,
    mut spawn_ship_event: EventWriter<SpawnShipEvent>,
) {
    let idx = hex::to_idx(mouse_pos.0);
    let hex_pos = hex::HexCell::new(idx.x as i32, idx.y as i32, 4);
    match q.single_mut() {
        Ok(res) => {
            let mut ship_t = res;
            ship_t.translation = hex_pos.pos;
        }
        Err(_e) => {
            // TODO: Create Ship Texture struct
            next_ship.send(NextShipEvent);
        }
    }
    if mouse_button_input.just_pressed(MouseButton::Left) {
        // TODO: CHECK FOR COLLISIONS BEFORE PLACING A NEW SHIP ONTO MAP
        for e in entity.iter() {
            commands.entity(e).despawn();
        }
        let dir = ShipDirEnum::DownLeft;
        // TODO: Create Ship Texture struct
        let ship_tex: Handle<Texture> = asset_server.get_handle("GameHex/ship_2tiles_dwl.png");

        spawn_ship_event.send(SpawnShipEvent {
            ship_pos: hex_pos.pos,
            ship_dir: dir,
            ship_texture: ship_tex,
            ship_type: ShipEnum::FastShip,
        });
    }
}

pub fn spawn_ship(
    mut commands: Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut spawn_ship_event: EventReader<SpawnShipEvent>,
) {
    for ev in spawn_ship_event.iter() {
        let ship_bundle = new_ship(ev.ship_type.clone(), ev.ship_dir);
        commands
            .spawn_bundle(SpriteBundle {
                material: materials.add(ev.ship_texture.clone().into()),
                transform: Transform::from_translation(ev.ship_pos),
                ..Default::default()
            })
            .insert_bundle(ship_bundle);
    }
}

pub fn spawn_ship_hexes(
    mut commands: Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
    tiles: Res<Tile>,
    mut spawn_ship_event: EventReader<SpawnShipEvent>,
) {
    // TODO! after spawning, all hexes must be bound to a single entity
    for ev in spawn_ship_event.iter() {
        let dir = ev.ship_dir;
        let size = ship_hex_size(&ev.ship_type);
        let mut hex = HexCell::new_from_pos(ev.ship_pos);
        for _i in 0..size {
            commands.spawn_bundle(SpriteBundle {
                material: materials.add(tiles.selected.clone().into()),
                transform: Transform::from_translation(hex.pos),
                ..Default::default()
            });
            match ev.ship_dir {
                ShipDirEnum::UpLeft => hex.move_lower_right(),
                ShipDirEnum::UpRight => hex.move_lower_left(),
                ShipDirEnum::Right => hex.move_left(),
                ShipDirEnum::DownRight => hex.move_upper_right(),
                ShipDirEnum::DownLeft => hex.move_upper_right(),
                ShipDirEnum::Left => hex.move_right(),
            }
        }
    }
}

pub fn movement() {
    println!("in game!");
}
