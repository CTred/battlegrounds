use bevy::{prelude::*, render::camera::Camera};

use crate::{
    entities::{
        camera::MainCamera,
        hex::{self, *},
    },
    helpers, LastIdx, MouseWorldPos, Selected,
};

pub fn mouse_interact(
    mouse_world_pos: Res<MouseWorldPos>,
    mouse_button_input: Res<Input<MouseButton>>,
    selected: Query<Entity, With<Selected>>,
    mut last_selected: ResMut<LastIdx>,
    mut commands: Commands,
) {
    if mouse_button_input.just_pressed(MouseButton::Left) {
        for e in selected.iter() {
            commands.entity(e).despawn();
        }
        let idx = hex::to_idx(mouse_world_pos.0);

        if idx != last_selected.0 {
            // let water_tex = asset_server.get_handle("GameHex/tileWater_gray.png");
            // let cell = HexCell::new(idx.x as i32, idx.y as i32, 1);
            // commands
            //     .spawn_bundle(SpriteBundle {
            //         material: materials.add(water_tex.into()),
            //         transform: Transform::from_translation(cell.pos),
            //         ..Default::default()
            //     })
            //     .insert(Selected);
            // last_selected.0 = idx;
        } else {
            // last_selected.0 = Vec2::new(1000., 1000.);
        }
    }
}

pub fn mouse_world_pos(
    windows: Res<Windows>,
    camera_query: Query<(&Transform, &Camera), With<MainCamera>>,
    mut mouse_pos: ResMut<MouseWorldPos>,
) {
    let window = windows.get_primary().unwrap();
    if let Some(cursor) = window.cursor_position() {
        mouse_pos.0 = helpers::camera::cursor_to_world(cursor, &camera_query, window)
    }
}

pub fn kb_interact(
    keyboard_input: Res<Input<KeyCode>>,
    mut query: Query<&mut Transform, With<Selected>>,
) {
    for mut transform in query.iter_mut() {
        let mut cell = HexCell::new_from_pos(Vec3::new(
            transform.translation.x,
            transform.translation.y,
            0.,
        ));

        if keyboard_input.pressed(KeyCode::L) {
            cell.move_right();
            println!("{:?}", cell.pos);
            transform.translation = cell.pos;
        }

        if keyboard_input.pressed(KeyCode::J) {
            cell.move_left();
            println!("{:?}", cell.pos);
            transform.translation = cell.pos;
        }

        if keyboard_input.pressed(KeyCode::I) {
            cell.move_upper_left();
            println!("{:?}", cell.pos);
            transform.translation = cell.pos;
        }

        if keyboard_input.pressed(KeyCode::O) {
            cell.move_upper_right();
            println!("{:?}", cell.pos);
            transform.translation = cell.pos;
        }

        if keyboard_input.pressed(KeyCode::M) {
            cell.move_lower_left();
            println!("{:?}", cell.pos);
            transform.translation = cell.pos;
        }

        if keyboard_input.pressed(KeyCode::Comma) {
            cell.move_lower_right();
            println!("{:?}", cell.pos);
            transform.translation = cell.pos;
        }
    }
}
