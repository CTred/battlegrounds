mod entities;
mod helpers;
mod systems;

use bevy::{prelude::*, window::WindowMode};

use crate::systems::interaction;
use entities::{
    camera::MainCamera,
    hex::HexCell,
    ship::{Ship, ShipEnum},
};
use helpers::camera;
use systems::ship_systems;

const ARENA_WIDTH: u32 = 12;
const ARENA_HEIGHT: u32 = 8;
const MAP_SPAN: i32 = 12;

pub struct Tile {
    pub blue: Handle<Texture>,
    pub selected: Handle<Texture>,
    pub damage: Handle<Texture>,
}
impl FromWorld for Tile {
    fn from_world(world: &mut World) -> Self {
        let asset_server = world.get_resource_mut::<AssetServer>().unwrap();
        let blue = asset_server.get_handle("GameHex/hex_blue.png");
        let selected = asset_server.get_handle("GameHex/hex_selected.png");
        let damage = asset_server.get_handle("GameHex/hex_damage.png");
        Tile {
            blue,
            selected,
            damage,
        }
    }
}

pub struct ShipList(Vec<ShipEnum>);
impl FromWorld for ShipList {
    fn from_world(_world: &mut World) -> Self {
        ShipList(vec![
            ShipEnum::FastShip,
            ShipEnum::FastShip,
            ShipEnum::Submarine,
            ShipEnum::Destroyer,
        ])
    }
}

pub struct ShipDirVal(i32);
// #[derive(Default)]
// pub struct Hex;
pub struct Selected;

#[derive(Default)]
pub struct LastIdx(Vec2);

#[derive(Default)]
pub struct MouseWorldPos(Vec2);

#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub enum GameState {
    Menu,
    Preparation,
    InGame,
    Paused,
}

fn main() {
    App::build()
        .insert_resource(WindowDescriptor {
            title: "Battlegrounds".to_string(),
            width: 1024.,
            height: 720.,
            vsync: false,
            resizable: true,
            mode: WindowMode::Windowed,
            ..Default::default()
        })
        // General Initialization
        .add_plugins(DefaultPlugins)
        .add_startup_system(setup.system())
        .insert_resource(ClearColor(Color::rgb(0.1, 0.1, 0.1)))
        .init_resource::<Tile>()
        .init_resource::<ShipList>()
        .insert_resource(ShipDirVal(0))
        .add_startup_system(build_world.system())
        .add_event::<ship_systems::SpawnShipEvent>()
        .add_event::<ship_systems::NextShipEvent>()
        // Initial State
        .add_state(GameState::Preparation)
        // Menu State
        // Preparation State
        .add_system_set(
            SystemSet::on_update(GameState::Preparation)
                .with_system(ship_systems::hover_with_ship.system())
                .with_system(ship_systems::spawn_ship.system())
                .with_system(ship_systems::spawn_ship_hexes.system())
                .with_system(ship_systems::rotate_ship.system())
                .with_system(ship_systems::spawn_next_ship.system()),
        )
        // InGame State
        .add_system_set(
            // TODO: below is actually on_update
            SystemSet::on_enter(GameState::InGame).with_system(ship_systems::movement.system()),
        )
        // Paused State
        .add_system(camera::movement.system())
        .add_system(interaction::mouse_interact.system())
        .add_system(interaction::kb_interact.system())
        .add_system(interaction::mouse_world_pos.system())
        .run();
    println!("Move Right");
}

fn setup(mut commands: Commands, asset_server: Res<AssetServer>) {
    let scenes: Vec<HandleUntyped> = asset_server
        .load_folder("GameHex")
        .expect("did not find game assets");

    commands.insert_resource(scenes);
    commands
        .spawn_bundle(OrthographicCameraBundle::new_2d())
        .insert(MainCamera);
    commands.spawn_bundle(UiCameraBundle::default());
    commands.insert_resource(MouseWorldPos::default());
    commands.insert_resource(LastIdx::default());
}

fn build_world(
    mut commands: Commands,
    tiles: Res<Tile>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    for i in -MAP_SPAN..=MAP_SPAN {
        for j in -MAP_SPAN..=MAP_SPAN {
            let cell = HexCell::new(i, j, 0);
            commands.spawn_bundle(SpriteBundle {
                material: materials.add(tiles.blue.clone().into()),
                transform: Transform::from_translation(cell.pos),
                ..Default::default()
            });
        }
    }
    let cell = HexCell::new(0, 0, 1);
    commands
        .spawn_bundle(SpriteBundle {
            material: materials.add(tiles.selected.clone().into()),
            transform: Transform::from_translation(cell.pos),
            ..Default::default()
        })
        .insert(Selected);
}
