use bevy::math::Vec3;
use bevy::prelude::*;

pub const HEX_WIDTH: f32 = 58.;
pub const HEX_HEIGHT: f32 = 86.;

#[derive(Debug)]
pub struct HexCell {
    pub pos: Vec3,
    // pub outer_radius: f32,
}
impl HexCell {
    pub fn new(x: i32, y: i32, layer: i32) -> Self {
        let pos: Vec3 = Vec3::new(
            x as f32 * HEX_WIDTH + (y.abs() as f32 % 2.) * (0.5 * HEX_WIDTH),
            (y as f32 / 2.) * HEX_HEIGHT,
            layer as f32,
        );
        HexCell { pos }
    }
    pub fn new_from_pos(index: Vec3) -> Self {
        let pos: Vec3 = Vec3::new(index.x, index.y, index.z);
        HexCell { pos }
    }
    pub fn move_right(&mut self) {
        self.pos.x += HEX_WIDTH;
    }
    pub fn move_left(&mut self) {
        self.pos.x -= HEX_WIDTH;
    }
    pub fn move_upper_left(&mut self) {
        self.pos.x -= 0.5 * HEX_WIDTH;
        self.pos.y += 0.5 * HEX_HEIGHT;
    }
    pub fn move_upper_right(&mut self) {
        self.pos.x += 0.5 * HEX_WIDTH;
        self.pos.y += 0.5 * HEX_HEIGHT;
    }
    pub fn move_lower_left(&mut self) {
        self.pos.x -= 0.5 * HEX_WIDTH;
        self.pos.y -= 0.5 * HEX_HEIGHT;
    }
    pub fn move_lower_right(&mut self) {
        self.pos.x += 0.5 * HEX_WIDTH;
        self.pos.y -= 0.5 * HEX_HEIGHT;
    }
    // pub fn inner_radius(&self) -> f32 {
    //     inner_radius(self.outer_radius)
    // }
}

fn inner_radius(outer_radius: f32) -> f32 {
    (outer_radius / 2.0) * (3.0_f32).sqrt()
}

pub fn to_idx(pos: Vec2) -> Vec2 {
    let y = ((pos.y) * 2. / HEX_HEIGHT).round();
    let x = ((pos.x - (y.abs() % 2.) * 0.5 * HEX_WIDTH) / HEX_WIDTH).round();
    Vec2::new(x, y)
}

// FIXME: HexCorners may not be necessary, hence not fully implemented. need to add pos.x, pos.y and pos.z to all corners to get global_pos.
// NOTE: not sure if corners should be in global_pos or local_pos.
struct HexCorners {
    top: Vec3,
    top_right: Vec3,
    bottom_right: Vec3,
    bottom: Vec3,
    bottom_left: Vec3,
    top_left: Vec3,
}
fn corners(pos: Vec3, outer_radius: f32) -> HexCorners {
    HexCorners {
        top: Vec3::new(pos.x, pos.y, pos.z + outer_radius),
        top_right: Vec3::new(inner_radius(outer_radius), 0., 0.5 * outer_radius),
        bottom_right: Vec3::new(inner_radius(outer_radius), 0., -0.5 * outer_radius),
        bottom: Vec3::new(0., 0., -outer_radius),
        bottom_left: Vec3::new(-inner_radius(outer_radius), 0., -0.5 * outer_radius),
        top_left: Vec3::new(-inner_radius(outer_radius), 0., 0.5 * outer_radius),
    }
}
