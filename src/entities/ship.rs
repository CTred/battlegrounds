use bevy::prelude::*;

use super::hex::HexCell;

pub enum Player {
    One,
    Two,
    Three,
    Four,
}

pub struct ShipNotPlaced;
pub struct Ship(pub u32);
pub struct Name(pub String);
pub struct Health(pub i32);
pub struct Movement(pub u32);
pub struct LoS(pub u32);
pub struct Weapon {
    pub damage: i32,
    pub range: u32,
    pub radius: i32,
}

#[derive(Bundle)]
pub struct ShipBundle {
    pub name: Name,
    pub health: Health,
    pub movement: Movement,
    pub direction: ShipDirEnum,
    pub los: LoS,
    pub weapon: Weapon,
    pub _p: Player,
    pub _s: Ship,
}

impl ShipBundle {
    pub fn fast_ship(dir: ShipDirEnum) -> Self {
        ShipBundle {
            name: Name("my ship".to_owned()),
            health: Health(3),
            movement: Movement(3),
            direction: dir,
            los: LoS(3),
            weapon: Weapon {
                damage: 3,
                range: 5,
                radius: 5,
            },
            _p: Player::One,
            _s: Ship(2),
        }
    }
}

#[derive(Clone, Copy)]
pub enum ShipDirEnum {
    UpLeft,
    UpRight,
    Right,
    DownRight,
    DownLeft,
    Left,
}

#[derive(Clone, Copy, Debug)]
pub enum ShipEnum {
    FastShip,
    Submarine,
    Destroyer,
}

pub fn new_ship(ship: ShipEnum, dir: ShipDirEnum) -> ShipBundle {
    match ship {
        ShipEnum::FastShip => ShipBundle::fast_ship(dir),
        ShipEnum::Submarine => ShipBundle::fast_ship(dir),
        ShipEnum::Destroyer => ShipBundle::fast_ship(dir),
    }
}

pub fn ship_water(ship: &Ship, ref_idx: &HexCell, dir: ShipDirEnum) -> Vec<Vec3> {
    match ship {
        Ship(1) => new_ship_water_dir(ref_idx.pos, 1, dir),
        Ship(2) => new_ship_water_dir(ref_idx.pos, 2, dir),
        Ship(3) => new_ship_water_dir(ref_idx.pos, 3, dir),
        _ => panic!("ship size not implemented"),
    }
}

pub fn ship_hex_size(ship: &ShipEnum) -> u32 {
    match ship {
        ShipEnum::FastShip => 2,
        ShipEnum::Submarine => 3,
        ShipEnum::Destroyer => 4,
    }
}

fn new_ship_water_dir(init_pos: Vec3, size: u32, dir: ShipDirEnum) -> Vec<Vec3> {
    let mut res = Vec::<Vec3>::new();
    match dir {
        ShipDirEnum::DownLeft => {
            for p in 0..size {
                res.push(Vec3::new(
                    init_pos.x + p as f32,
                    init_pos.y + p as f32,
                    init_pos.z,
                ))
            }
        }
        ShipDirEnum::Left => {
            for p in 0..size {
                res.push(Vec3::new(
                    init_pos.x + p as f32,
                    init_pos.y + p as f32,
                    init_pos.z,
                ))
            }
        }
        ShipDirEnum::UpLeft => {
            for p in 0..size {
                res.push(Vec3::new(
                    init_pos.x + p as f32,
                    init_pos.y + p as f32,
                    init_pos.z,
                ))
            }
        }
        ShipDirEnum::UpRight => {
            for p in 0..size {
                res.push(Vec3::new(
                    init_pos.x + p as f32,
                    init_pos.y + p as f32,
                    init_pos.z,
                ))
            }
        }
        ShipDirEnum::Right => {
            for p in 0..size {
                res.push(Vec3::new(
                    init_pos.x + p as f32,
                    init_pos.y + p as f32,
                    init_pos.z,
                ))
            }
        }
        ShipDirEnum::DownRight => {
            for p in 0..size {
                res.push(Vec3::new(
                    init_pos.x + p as f32,
                    init_pos.y + p as f32,
                    init_pos.z,
                ))
            }
        }
    }
    res
}
