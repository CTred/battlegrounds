use bevy::{input::mouse::MouseWheel, prelude::*, render::camera::Camera};

use crate::entities::camera::MainCamera;

// A simple camera system for moving and zooming the camera.
pub fn movement(
    time: Res<Time>,
    keyboard_input: Res<Input<KeyCode>>,
    mut mouse_event: EventReader<MouseWheel>,
    mut query: Query<&mut Transform, With<Camera>>,
) {
    for mut transform in query.iter_mut() {
        let mut direction = Vec3::ZERO;
        let scale = transform.scale.x;

        if keyboard_input.pressed(KeyCode::A) {
            direction -= Vec3::new(1.0, 0.0, 0.0);
        }

        if keyboard_input.pressed(KeyCode::D) {
            direction += Vec3::new(1.0, 0.0, 0.0);
        }

        if keyboard_input.pressed(KeyCode::W) {
            direction += Vec3::new(0.0, 1.0, 0.0);
        }

        if keyboard_input.pressed(KeyCode::S) {
            direction -= Vec3::new(0.0, 1.0, 0.0);
        }

        if keyboard_input.pressed(KeyCode::Z) {
            let scale = scale + 0.1;
            transform.scale = Vec3::splat(scale);
        }

        if keyboard_input.pressed(KeyCode::X) {
            let scale = scale - 0.1;
            transform.scale = Vec3::splat(scale);
        }

        for event in mouse_event.iter() {
            let scale = scale - event.y * 0.1;
            info!("{:?}", scale);
            transform.scale = Vec3::splat(scale);
        }

        if transform.scale.x < 1.0 {
            transform.scale = Vec3::splat(1.0)
        }

        transform.translation += time.delta_seconds() * direction * 500.;
    }
}

pub fn cursor_to_world(
    cursor: Vec2,
    camera_query: &Query<(&Transform, &Camera), With<MainCamera>>,
    window: &Window,
) -> Vec2 {
    let (transform, camera) = camera_query.single().expect("camera not found");

    let screen_size = Vec2::new(window.width() as f32, window.height() as f32);
    let camera_position = transform.compute_matrix();
    let projection_matrix = camera.projection_matrix;

    // Normalized device coordinate cursor position from (-1, -1, -1) to (1, 1, 1)
    let cursor_ndc = (cursor / screen_size) * 2.0 - Vec2::from([1.0, 1.0]);
    // let cursor_pos_ndc_near = cursor_ndc.extend(-1.0);
    let cursor_pos_ndc_far = cursor_ndc.extend(1.0);

    let ndc_to_world = camera_position * projection_matrix.inverse();
    // let cursor_pos_near = ndc_to_world.project_point3(cursor_pos_ndc_near);
    let cursor_pos_far = ndc_to_world.project_point3(cursor_pos_ndc_far);
    // let ray_direction = cursor_pos_far - cursor_pos_near;

    cursor_pos_far.truncate()
}
